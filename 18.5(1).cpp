﻿// 18.5(1).cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;

//Create my own stack class.
template <class AnyType>
class MyStack {
private:
	//Encapsulated vars.
	AnyType* StackArray = NULL;
	int size = 0;
	int top = 0;



	//Copy old array to temp. So we can expand/decrease array.


public:

	MyStack()
	{

	}

	~MyStack()
	{
		delete[] StackArray;
	}

	void DynamicArrayLogic()
	{
		AnyType* TempArray = new AnyType[size];//TempArray
		//Copy values to temp array
		for (int i = 0; i < size; i++)
		{
			TempArray[i] = StackArray[i];
		}
		delete[] StackArray;// Free old array memory
		StackArray = TempArray;//Now StackArray Pointer to new/temp array.
	}

	//Add an element
	void push(AnyType value)
	{
		// size++;//size + 1
		// if (StackArray==NULL) StackArray = new AnyType[size];//Allocate StackArray
		// DynamicArrayLogic();
		// StackArray[top++] = value;//Initialize element

		AnyType* NewArray = new AnyType[size + 1];
		cout << "Add Element " << value << endl;

		for (int i = 0; i < size; i++)
		{
			NewArray[i] = StackArray[i];
		}
		NewArray[size] = value;
		delete[] StackArray;
		size++;
		StackArray = NewArray;
	}

	//remove top element
	void pop()
	{
		--size;//Size -1
		// DynamicArrayLogic();
		// --top;//Top -1
	}

	//Print the stack
	void Print()
	{
		for (int i = 0; i < size; i++)
		{
			std::cout << *(StackArray + i) << " ";
		}
		std::cout << std::endl;
	}


};//End of MyStack Class

int main()
{
	MyStack<int> IntStack;
	MyStack<string> StringStack;

	//Test IntStack
	/*for (int i = 0; i < 10; i++)
	{*/
	IntStack.push(1);
	IntStack.push(2);
	IntStack.push(3);
	IntStack.push(4);
	IntStack.push(5);
	/*}*/
	IntStack.Print();
	IntStack.pop();
	IntStack.Print();
	IntStack.pop();
	IntStack.Print();
	IntStack.pop();
	IntStack.Print();
	IntStack.push(100);
	IntStack.Print();

	cout << "----------------" << endl;

	//I get a memory exception when pushing more then 3 strings. IntStack works fine. Maybe reaching memory limit?
	//Test StringStack
	StringStack.push("Test:0");
	StringStack.push("Test:1");
	StringStack.push("Test:2");
	StringStack.push("Test:3");
	StringStack.push("Test:4");
	StringStack.pop();
	StringStack.Print();
	StringStack.pop();
	StringStack.Print();
	StringStack.push("itsWork:1");
	StringStack.Print();
}


